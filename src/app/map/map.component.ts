import {Component, Input, OnInit} from '@angular/core';
import * as mapboxgl from "mapbox-gl";
import {environment} from "../../environments/environment";
import {Measurement} from "../model";

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.sass']
})
export class MapComponent implements OnInit {

    @Input()
    measurements: Measurement[]

    map: mapboxgl.Map;

    constructor() {
    }

    ngOnInit(): void {
        this.map = new mapboxgl.Map({
            accessToken: environment.mapbox.accessToken,
            container: 'map',
            style: 'mapbox://styles/mapbox/streets-v11',
            zoom: 13,
            center: [10.390825388611354, 55.39922645680075]
        });
        // Add map controls
        this.map.addControl(new mapboxgl.NavigationControl());

        for (let m of this.measurements) {
            new mapboxgl.Marker().setLngLat(new mapboxgl.LngLat(m.lng, m.lat)).addTo(this.map);
        }

    }

}
