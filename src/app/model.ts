export interface Device {
    id: string
    name: string
    measurements: Measurement[]
}

export interface Measurement {
    time: number
    lat: number
    lng: number
    temp: number
    hum: number
    shake: boolean
    tilt: boolean
}