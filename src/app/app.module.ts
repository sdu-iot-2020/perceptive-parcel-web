import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MapComponent} from './map/map.component';
import {DevicesComponent} from './devices/devices.component';
import {HttpClientModule} from "@angular/common/http";
import {DeviceComponent} from './device/device.component';
import {StatsComponent} from './stats/stats.component';
import {ChartsModule} from "ng2-charts";

@NgModule({
    declarations: [
        AppComponent,
        MapComponent,
        DevicesComponent,
        DeviceComponent,
        StatsComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatToolbarModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatIconModule,
        HttpClientModule,
        ChartsModule

    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
