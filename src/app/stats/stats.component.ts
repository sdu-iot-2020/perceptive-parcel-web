import {Component, Input, OnInit} from '@angular/core';
import {ChartDataSets} from "chart.js";
import {Measurement} from "../model";

@Component({
    selector: 'app-stats',
    templateUrl: './stats.component.html',
    styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

    @Input()
    measurements: Measurement[]

    data: ChartDataSets[];

    readonly chartType = 'line';
    readonly options = {
        scaleShowVerticalLines: false,
        responsive: true,
        scales: {
            // We use this empty structure as a placeholder for dynamic theming.
            xAxes: [{type: 'time',}],
            yAxes: [
                {
                    id: 'y-axis-0',
                    position: 'left',
                },
                {
                    id: 'y-axis-1',
                    position: 'right',
                }
            ]
        },
    };

    constructor() {
    }

    static toDateTemp(value: Measurement) {
        return {
            x: new Date(value.time),
            y: value.temp,
        }
    }

    static toDateHumid(value: Measurement) {
        return {
            x: new Date(value.time),
            y: value.hum,
        }
    }

    ngOnInit(): void {
        this.data = [
            {
                data: this.measurements.map(StatsComponent.toDateTemp), label: 'Temperature'
            },
            {
                data: this.measurements.map(StatsComponent.toDateHumid), label: 'Humidity', yAxisID: 'y-axis-1'
            },
        ]
    }

}
