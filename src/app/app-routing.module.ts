import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DevicesComponent} from "./devices/devices.component";
import {DeviceComponent} from "./device/device.component";


const routes: Routes = [
    {path: 'devices', component: DevicesComponent},
    {path: 'device/:id', component: DeviceComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
