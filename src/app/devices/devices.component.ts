import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";


@Component({
    selector: 'app-devices',
    templateUrl: './devices.component.html',
    styleUrls: ['./devices.component.sass']
})
export class DevicesComponent implements OnInit {

    readonly url = "https://perceptive-parcel.herokuapp.com/devices";

    list: Observable<string[]>

    constructor(private http: HttpClient) {
    }


    ngOnInit() {
        this.list = this.http.get<string[]>(this.url)
    }
}
