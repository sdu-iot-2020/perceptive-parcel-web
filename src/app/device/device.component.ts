import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {catchError, switchMap} from "rxjs/operators";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Device} from "../model";

@Component({
    selector: 'app-device',
    templateUrl: './device.component.html',
    styleUrls: ['./device.component.sass']
})
export class DeviceComponent implements OnInit {

    device: Observable<Device>

    readonly url = "https://perceptive-parcel.herokuapp.com/devices/";

    constructor(private route: ActivatedRoute, private http: HttpClient) {
    }

    private static handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            console.error('An error occurred:', error.error.message);
        } else {
            console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
        }
        return throwError('Something bad happened; please try again later.');
    };

    ngOnInit() {
        this.device = this.route.paramMap
            .pipe(switchMap((params: ParamMap) => this.http.get<Device>(this.url + params.get("id"))));
    }

    locate(id) {
        console.log(id)
        this.http.post("https://perceptive-parcel.herokuapp.com/locate", {"deviceId": +id})
            .pipe(catchError(DeviceComponent.handleError))
            .subscribe();
    }

}
