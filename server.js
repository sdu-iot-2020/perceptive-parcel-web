const express = require('express');
// noinspection JSUnusedLocalSymbols
const path = require('path'); // required anyway

const app = express();

app.use(express.static('./dist/perceptive-parcel-web/'));

app.get('/*', function (req, res) {
    res.sendFile('index.html', { root: 'dist/perceptive-parcel-web/' });
});

app.listen(process.env.PORT || 8080);
